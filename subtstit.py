import argparse
import os
import sys

import cv2
import numpy as np

parser = argparse.ArgumentParser()
parser.add_argument("folder", help="Image folder name", type=str)
parser.add_argument("-o", "--overlap", help="Overlap ratio percentage 0-100 (default 15)", type=int, default=15)
parser.add_argument("-d", "--predrop", help="Pre-drop n pixels from the bottom (default 0)", type=int, default=0)
parser.add_argument("--noshow", help="No display on screen", action="store_true", default=False)
args = parser.parse_args()
if not args.folder.endswith('/'):
    args.folder = args.folder + '/'

image_folder = args.folder
overlap_ratio = args.overlap / 100
export_file = 'export.jpg'

if os.path.exists(image_folder):
    image_files = os.listdir(image_folder)
    image_files = [file for file in image_files if file.endswith('.jpg') or file.endswith('.png')]
    if export_file in image_files:
        image_files.remove(export_file)
    file_count = len(image_files)
    if file_count == 0:
        error_msg = 'No image files found in folder' + image_folder + ' Not Found'
        sys.exit()
    print("{} image files found in folder {}".format(file_count, image_folder))
else:
    error_msg = 'Folder ' + image_folder + ' Not Found'
    sys.exit()

image_files.sort(reverse=True)

looping = True

while looping:
    for i in range(file_count):
        img_frame = cv2.imread(image_folder + image_files[i])
        if i == 0:
            height = img_frame.shape[0] - args.predrop
            width = img_frame.shape[1]
            step_height = int(height * overlap_ratio)
            final_height = height + step_height * (file_count - 1)
            img_out = np.zeros((final_height, width, 3), dtype=np.uint8)
            print("Input image dimension: {} x {}".format(width, height))
            print("Output image dimension: {} x {}".format(width, final_height))
            width_disp = 960
            height_disp = int(width_disp / width * final_height)
        start_row = (file_count - i - 1) * step_height
        end_row = start_row + height
        img_out[start_row:end_row, :, :] = img_frame[0:height, :, :]
    img_disp = cv2.resize(img_out, (width_disp, height_disp))
    key_input = ord('s')
    if not args.noshow:
        cv2.imshow('Output', img_disp)
        key_input = cv2.waitKey(0) & 0xFF
        print('Output image prepared. Press -/+ to adjust the overlap')
        print('Press S to save; Press Esc to exit')
    if key_input == ord('-') or key_input == ord('_'):
        overlap_ratio -= 0.01
    elif key_input == ord('+') or key_input == ord('='):
        overlap_ratio += 0.01
    else:
        if key_input == ord('s') or key_input == ord('S'):
            cv2.imwrite(image_folder + export_file, img_out)
            print('Image file saved as {}'.format(export_file))
        looping = False

if not args.noshow:
    cv2.destroyAllWindows()
